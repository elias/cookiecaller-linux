import org.gnome.gtk.Gtk;
import org.gnome.gtk.Menu;
import org.gnome.gtk.MenuItem;
import org.gnome.gtk.StatusIcon;

public class TrayIcon implements Runnable{

    StatusIcon trayIcon;

    Menu menu;
    MenuItem button;

    public TrayIcon(String appname, String tooltip){

        Gtk.init(null);

        trayIcon = new StatusIcon();
        trayIcon.setFromFile("resources/favicon.ico");
        trayIcon.setTooltipText(appname + tooltip);

        menu = new Menu();
        button = new MenuItem("Beenden");
    }
    @Override
    public void run() {
        button.connect(new MenuItem.Activate() {
            @Override
            public void onActivate(MenuItem menuItem) {
                System.exit(0);
            }
        });

        menu.append(button);
        trayIcon.connect(new StatusIcon.PopupMenu() {
            @Override
            public void onPopupMenu(StatusIcon statusIcon, int i, int i1) {
                menu.popup(statusIcon);
            }
        });

        menu.showAll();

        Gtk.main();
    }
}
