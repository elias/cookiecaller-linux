import org.gnome.notify.Notification;
import org.gnome.notify.Notify;
import org.gnome.notify.Urgency;

public class Benachrichtigung{

    public Benachrichtigung(String appname){
        Notify.init(appname);
    }

    public void sendNotification(String name, String nachricht){
        Notification notification = new Notification(name, nachricht, "resources/icon.png");
        notification.setCategory("im.received");
        notification.setUrgency(Urgency.NORMAL);
        notification.show();
    }

}
